# gw_pin
<B>Configuration</B>
Configuration is stored in conf file "gw_pin.conf" by default.
```
gw_count        # number of MAC, PIN pair to be generated
output_file     # output file path
api_endpoint    # address of the api
id_default      # first half of the MAC if the output file is missing
id_type         # second half of the MAC
```


<B>Functions</B>
```
get_config(config_file)

returns the configuration dictionary

@config_file
path to the configuration file
```
```
get_last_id(output_file)

returns the last MAC (upper half) found in the output file

@output_file
path to the output file
```
```
create_output_file(output_file)

creates the empty output file with the header

@output_file
path to the output file
```
```
get_pin(api, id_start, id_type, count)

returns list of (MAC,PIN) tuples, retrieved from the api

@api
url of the api

@id_start
first MAC address (upper half)

@id_type
lower half of the MAC address

@count
number of the MAC addresses to be retrieved
```
```
write_pin(gw_list, output_file)

appends the (MAC,PIN) tuples to the output file

@gw_list
list of the (MAC,PIN) tuples

@output_file
path to the output file
```
