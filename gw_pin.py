import requests
import json
import csv
import configparser


CONFIG_FILE = "gw_pin.conf"


def get_config(config_file: str) -> dict:

    config = configparser.ConfigParser()
    config.read(config_file)
    config = config['DEFAULT']
    return config


def get_last_id(output_file: str) -> str:

    try:
        with open(output_file, 'r') as file:
            reader = csv.reader(file)
            rows = list(reader)
            return(rows[-1][0][:8])
    except FileNotFoundError:
        return None


def create_output_file(output_file: str):

    with open(output_file, 'w') as file:
        writer = csv.writer(file , lineterminator='\n')
        header = ("MAC", "PIN",)
        writer.writerow(header)


def get_pin(api: str, id_start: int, id_type: str, count: int) -> list:

    assert(count > 0)

    gw_list = []
    for id_cnt in range(id_start, id_start + count):
        gw_id = f"{id_cnt:x}{id_type}"
        assert(len(gw_id) == 16)

        request = {"gatewayId":gw_id,}
        response = requests.post(url=api, json=request) 
        response = json.loads(response.text)

        gw_list.append((request["gatewayId"], response["longPin"]))
    return gw_list


def write_pin(gw_list: list, output_file: str):

    with open(output_file, 'a') as file:
        writer = csv.writer(file, lineterminator='\n')    
        for gw in gw_list:
            writer.writerow(gw)


def main():

    config = get_config(CONFIG_FILE)
    output_file = config['output_file']
    id_last = get_last_id(output_file)

    if not id_last:
        create_output_file(output_file)
        id_last = config['id_default']

    id_start = int(id_last, 16) + 1
    count = int(config['gw_count'])
    gw_list = get_pin(config['api_endpoint'], id_start, config['id_type'], count)
    write_pin(gw_list, output_file)


if __name__ == "__main__":
    main()
